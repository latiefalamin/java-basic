package com.secondstack.training.basic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 * @author Latief
 */
public class CollectionMapMain {

    /**
     * hello
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        Object [] datas = {"2","a","0","1","3","a",7,"d","0","6","7"};
        Integer [] datasInteger = {2,0,1,3,7,0,6,7};
        ArrayList list = new ArrayList();
        addCollectionValues(list, datas);
        showCollection(list);
        
        HashSet set = new HashSet();
        addCollectionValues(set, datas);
        showCollection(set);
        
        TreeSet treeSet = new TreeSet();
        addCollectionValues(treeSet, datasInteger);
        showCollection(treeSet);
        
        Map map = new HashMap();
        map.put(1, "Satu");
        map.put(2, "Dua");
        map.put(3, "Tiga");
        map.put(4, "Empat");
        map.put(5, "Lima");
        System.out.println("Map");
        System.out.println(map.get(1));
    }
    
    public static void addCollectionValues(Collection collection, Object [] datas){
        for(int i = 0;i<datas.length;i++){
            collection.add(datas[i]);
        }
    }
    
    public static void showCollection(Collection collection){
        System.out.println(collection.getClass());
        for(Object o:collection){
            System.out.print(o + " ");
        }
        System.out.println("\n");
    }
}