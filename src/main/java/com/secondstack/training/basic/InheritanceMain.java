package com.secondstack.training.basic;

import com.secondstack.training.basic.inheritance.Person;
import com.secondstack.training.basic.inheritance.Student;

/**
 *
 * @author Latief
 */
public class InheritanceMain {
    public static void main(String[] args) {
        Person joko = new Person();
        joko.setName("Joko");
        joko.setAlamat("Yogyakarta");
        showPerson(joko);
        
        Person siti = new Person("Siti", "Bandung");
        showPerson(siti);
        
        showPerson(new Person("Doni", "Semarang"));
        
        Student sahrul = new Student();
        sahrul.setName("Sahrul");
        sahrul.setAlamat("Yogyakarta");
        sahrul.setNoInduk("001");
        sahrul.setSekolah("SMA YOGYAKARTA");
        showStudent(sahrul);
        
        Person juwita = new Student();
        juwita.setName("Juwita");
        juwita.setAlamat("Yogyakarta");
        showPerson(juwita);
        showStudent((Student) juwita);
        
        Student studentJuwita = (Student) juwita;
        studentJuwita.setNoInduk("002");
        studentJuwita.setSekolah("SMA YOGYAKARTA");
        showStudent(studentJuwita);
    }
    
    public static void showPerson(Person person){
        System.out.println();
        System.out.println("Name : " + person.getName());
        System.out.println("Alamat : " + person.getAlamat());
    }
    
    public static void showStudent(Student student){
        showPerson(student);
        System.out.println("NoInduk : " + student.getNoInduk());
        System.out.println("Sekolah : " + student.getSekolah());
    }
}
