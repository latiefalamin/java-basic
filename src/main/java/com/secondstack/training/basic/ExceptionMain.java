package com.secondstack.training.basic;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Latief
 */
public class ExceptionMain {

    public static void main(String[] args) {
        try {
            System.out.println(3 / 0);
        } catch (ArithmeticException ex) {
            Logger.getLogger(ExceptionMain.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage(), ex.getClass().getSimpleName(), 
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}