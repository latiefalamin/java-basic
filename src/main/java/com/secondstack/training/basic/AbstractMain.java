package com.secondstack.training.basic;

import com.secondstack.training.basic.abstracts.Animal;
import com.secondstack.training.basic.abstracts.Cow;
import com.secondstack.training.basic.abstracts.Human;
import com.secondstack.training.basic.abstracts.LivingThing;
import com.secondstack.training.basic.abstracts.Mammalia;

/**
 *
 * @author Latief
 */
public class AbstractMain {

    /**
     * hello
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        Human human = new Human();
        Cow cow = new Cow();
        Mammalia kanggoroo = new Mammalia() {
            @Override
            public void walk() {
                System.out.println("Kanggoroo walks by jumping...");
            }

            @Override
            public void eat() {
                System.out.println("Kanggoroo Herbivora ...");
            }
        };

        describe(human);
        describe(cow);
        describe(kanggoroo);

        describe(new Animal() {
            @Override
            public void walk() {
                System.out.println("Fish cannot walk, but fish can swing in water ..");
            }

            public void breath() {
                System.out.println("Fish breath by gill ...");
            }

            public void eat() {
                System.out.println("Some fish is Herbivora, some the other is Carnivora ...");
            }
        });
    }

    public static void describe(LivingThing livingThing) {
        System.out.println("\n" + livingThing.getClass());
        livingThing.breath();
        livingThing.eat();
    }
}
