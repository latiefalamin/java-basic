package com.secondstack.training.basic.inheritance;

/**
 *
 * @author Latief
 */
public class Person {

    private String name;
    private String alamat;

    public Person() {}

    public Person(String name, String alamat) {
        this.name = name;
        this.alamat = alamat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override//overriding method
    public String toString() {
        return "Person{name:" + name + ",alamat:" + alamat + "}";
    }
}
