package com.secondstack.training.basic.inheritance;

/**
 *
 * @author Latief
 */
public class Student extends Person {
    private String noInduk;
    private String sekolah;

    public Student() {
        super();
    }

    public String getNoInduk() {
        return noInduk;
    }

    public void setNoInduk(String noInduk) {
        this.noInduk = noInduk;
    }

    public String getSekolah() {
        return sekolah;
    }

    public void setSekolah(String sekolah) {
        this.sekolah = sekolah;
    }

    @Override//overriding method
    public String toString() {
        return "Student{name:" + getName()
                + ",alamat:" + getAlamat()
                + ", noInduk:" + noInduk
                + ",sekolah:" + sekolah
                + "}";
    }
}
