package com.secondstack.training.basic.abstracts;

/**
 *
 * @author Latief
 */
public abstract class Animal implements LivingThing{
    public abstract void walk();
}
