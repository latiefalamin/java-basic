package com.secondstack.training.basic.abstracts;

/**
 *
 * @author Latief
 */
public class Cow extends Mammalia {

    @Override
    public void walk() {
        System.out.println("Cow walks with 4 legs ...");
    }

    @Override
    public void eat() {
        System.out.println("Cow Herbivora ...");
    }
}
