package com.secondstack.training.basic.abstracts;

/**
 *
 * @author Latief
 */
public class Human extends Mammalia{
    @Override
    public void eat() {
        System.out.println("Human Carnivora...");
    }
    @Override
    public void walk() {
        System.out.println("Human walks with 2 legs...");
    }
}
