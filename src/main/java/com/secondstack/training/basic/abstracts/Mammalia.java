package com.secondstack.training.basic.abstracts;

/**
 *
 * @author Latief
 */
public abstract class Mammalia implements LivingThing {

    @Override
    public void breath() {
        System.out.println("Living Thing breath by lungs ...");
    }

    public abstract void walk();
}
