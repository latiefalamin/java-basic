package com.secondstack.training.basic.abstracts;

/**
 *
 * @author Latief
 */
public class Plant implements LivingThing{

    @Override
    public void breath() {
        System.out.println("Plant breathing by leaf ...");
    }
    
    @Override
    public void eat() {
        System.out.println("Plant eat water and mineral from earth ...");
    }
}
