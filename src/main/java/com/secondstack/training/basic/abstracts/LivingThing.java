package com.secondstack.training.basic.abstracts;

/**
 *
 * @author Latief
 */
public interface LivingThing {
    void breath();
    void eat();
}
